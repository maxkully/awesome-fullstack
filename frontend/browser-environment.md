# [Browser Environment](browser-environment.md)

- ### HTTP
- ### DOM
- ### Storage
- ### Permissions
- ### Extensions
- ### AJAX
- ### CORS & Policies
