# [Frontend Roadmap](README.md)

### [Browser Environment](browser-environment.md)

- HTTP
- DOM
- Storage
- Permissions
- Extensions
- AJAX
- CORS
- Polgicies

### [Layout](layout.md)

- HTML
    - Basics
    - Semantic HTML
    - Forms and validations
    - Accessibility
    - SEO Basics
    - Conventions
    - Best practices
- CSS
    - Basics
    - Floats
    - Positioning
    - Display
    - Box model
    - CSS Grid
    - Flex box
    - Responsive
    - Media
- Cross-browser
- Templating
- CSS architecture
- CSS preprocessors
- Modern CSS
    - Styled component
    - CSS Module
    - Styled JSX
    - Emotion
- Web Components
    - Shadow DOM
    - Custom Elements
    - HTML Template
- CSS Frameworks
     - Reactstrap
     - Material UI
     - Talwind CSS
     - Chakira UI
     - Bootstrap

### VanillaJS

- Basic
    - DOM Manipulation
    - Fetch API
    - XHR
    - Hoisting
    - Event bubbling
    - Scope
    - Prototype
    - Shadow DOM
    - Strict
- ES6+
- TypeScript
- Specification

### Package managers

- npm
- yarn

### Build Tools

- npm
- webpack

### [Architecture](architecture.md)

- SPA

### Frameworks

- React.js
- Angular
- Vue.js

### UI/UX

### Marketing tools

### Performance

### Cross/Multi devices

- Mobile Applications
    - React Native
    - NativeScript
    - Flutter
    - Ionic
- Desktop Applications
    - Electron

### Web security

### [Auto testing](../qa/README.md)

### Type checkers

### Progressive Web Apps

- PRPL Pattern
- RAIL Model
- Performance metrics
- Using Lighthouse
- Using DevTools
- Storage
- WebSockets
- Service Workers
- Location
- Notifications
- Device orientation
- Payments
- Credentials

### Web API

### Server side rendering (SSR)

- Next.js
- Nuxt.js
- Universal

### [GraphQL](../backend/README.md#API)

### Static site generators

- Next.jx
- Gatsby.js
- Nuxt.jx
- Vuepress
- Jekyll
- Hugo
- Gridsome

### WebAssembly
