Knowledge base for senior full stack developers
===============================================

[Roadmap](roadmap.md)

Tiers
=====

### [Frontend](frontend/README.md)

- ##### [Browser Environment](frontend/browser-environment.md)
- ##### [Layout](frontend/layout.md)
- ##### VanillaJS
- ##### [Architecture](frontend/architecture.md)
- ##### Frameworks
- ##### WebSockets
- ##### WebWorkers
- ##### UI/UX
- ##### Marketing tools
- ##### Performance
- ##### Cross devices
- ##### Multi devices
 
---

### [Backend](backend/README.md)

- ##### Algorithms / Data structures
- ##### Design Patterns
- ##### OS
- ##### Networking
- ##### HTTP
- ##### SSL
- ##### Web servers
- ##### Architecture
- ##### API
- ##### Languages & Frameworks
- ##### Databases
- ##### Performance
- ##### Fault Tolerance
- ##### Specific tools

---

### [DevOps](devops/README.md)

- ##### Environments
- ##### CI/CD
- ##### Monitoring

---

### [QA](qa/README.md)

- ##### TDD
- ##### Acceptance tests
- ##### End-to-end tests
- ##### Specific tests

---

### [Security](security/README.md)

- ##### Vulnerabilities
- ##### Cryptography
- ##### ACL
- ##### Open Standards
- ##### Open Tools

---

References
==========

- https://github.com/kevindeasis/awesome-fullstack
- https://www.freecodecamp.org/news/2019-web-developer-roadmap/
