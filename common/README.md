## COMMON

### Index

- Internet
- Git
- Basic Terminal Usage (CLI)
- Data structures & algorithms
- Design principles
- GitHub / GitLab / BitBucket
- Licenses
- Semantic versioning
- SSH
- HTTP/HTTPS
- Design patterns
- Character Encoding
- Float numbers

### Internet

#### Agenda

- How does Internet work?
- What is HTTP?
- Browsers and how they work?
- DNS and how it works?
- What is Domain Name?
- What is Hosting?

#### Interview questions

- OSI model
- What is Stateless and Stateful?

#### Task

`@todo`

### Git

#### Agenda

- Overview
- clone
- checkout
- branch
- status
- stash
- log
- diff
- reset
- commit
- merge
- rebase
- fetch
- pull
- push

#### Interview questions

- What difference between merge and rebase?
- What is a squash?

#### Task

`@todo`

### Basic Terminal Usage

#### Agenda

- Shell
- Bash
- Zsh
- Oh my zsh
- Scripting
- GNU

#### Interview questions

`@todo`

#### Task

`@todo`

### Data structures, algorithms

#### Agenda

##### Data structures

[Wiki](https://en.wikipedia.org/wiki/List_of_data_structures)

- Arrays
- Lists
- Trees
- Graphs
- Hashes

##### Algorithms

[List of algorithms](https://en.wikipedia.org/wiki/List_of_algorithms)

- Complexity: O(n)-notation (Big O-notation)
- Sorting
- Searching

##### Design principles

- SOLID
- KISS
- DRY
- YAGNI
- GOF Design Patterns
- TDD
- DDD

#### Interview questions

`@todo`

#### Task

`@todo`

### GitHub / GitLab / BitBucket

#### Agenda

- SSH Keys

#### Interview questions

`@todo`

#### Task

`@todo` 

### Licenses

https://choosealicense.com/licenses/

#### Interview questions

`@todo`

#### Task

`@todo`

### Semantic versioning

https://semver.org/

#### Interview questions

`@todo`

#### Task

`@todo`

### SSH

#### Agenda

- Overview
- Specification
- OpenSSH
- Configuration
- Generation
- Tunnels

#### Interview questions

`@todo`

#### Task

`@todo`

### HTTP/HTTPS

[Documentation](https://developer.mozilla.org/en-US/docs/Web/HTTP)

#### Agenda

- Overview
    - URI
    - MIME Types
    - Message
    - Session
- Security
- Feature Policy
- [Specifications](https://developer.mozilla.org/en-US/docs/Web/HTTP/Resources_and_specifications)
- [HTTPS](https://tools.ietf.org/html/rfc2818)
- [HSTS](https://tools.ietf.org/html/rfc6797)

#### Interview questions

- What are differences between 1.0/1.1 protocols?
- What are CORS?

#### Task

- Implement web-server

### Design patterns

- MVC
- Singleton
- Factory
- Observable
- Facade
- Dependency Injection
- Dependency Inversion

### Character Encoding

https://en.wikipedia.org/wiki/Character_encoding

* ASCII
* ISO 8859
* Unicode

#### Tools

* iconv

#### Task

* write encode/decode from one to another standard

### Float numbers

https://en.wikipedia.org/wiki/IEEE_754

#### Interview questions

`@todo`

#### Task

`@todo`
