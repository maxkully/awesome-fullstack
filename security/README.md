# [Security](README.md)

- ## Vulnerabilities
  - #### Web application level
  - #### Networking level
  - #### Application level
  - #### Service level
  - #### HR level
  - #### Defencing
- ## Cryptography
  - #### Hashes
  - #### Encode/Decode
  - #### SSL/TLS
  - #### VPN
- ## ACL
  - #### LDAP
- ## Open Standards
  - #### OWASP
- ## Open Tools
  - #### [Metasploit](https://www.metasploit.com/)

---

References
==========

- https://github.com/pxlpnk/awesome-ruby-security
