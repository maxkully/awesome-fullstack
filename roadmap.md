WEB DEVELOPER ROADMAP
=====================

Based on [developer roadmap by kamranahmedse](https://github.com/kamranahmedse/developer-roadmap)
from [Roadmap.sh](https://roadmap.sh)

### [COMMON](common/README.md)
<details>
<summary>
Content    
</summary>
<p>
    
- Internet
- Git
- Basic Terminal Usage (CLI)
- Data structures & algorithms
- Design principles
- GitHub / GitLab / Bitbucket
- Licenses
- Semantic versioning
- SSH
- HTTP/HTTPS
- Design patterns
- Character Encoding
- Float numbers

</p>
</details>

### [FRONT-END](frontend/README.md)
<details>
<summary>
Content    
</summary>
<p>
    
- HTML
- CSS
- JavaScript
- Web security
- Package managers
- CSS architecture
- CSS preprocessors
- Build tools
- JS Frameworks
- Modern CSS
- Web Components
- CSS Frameworks
- Auto testing
- Type checkers
- Progressive Web Apps
- Server side rendering (SSR)
- GraphQL
- Static site generators
- Mobile applications
- Desktop applications
- Web assembly

</p>
</details>

### [BACK-END](backend/README.md)
<details>
<summary>
Content    
</summary>
<p>
    
- OS
- Languages
- Relational DB
- NoSQL
- API
- Caching
- Web security
- Testing
- CI/CD
- Design principles
- Development principles
- Architectural patterns
- Search engines
- Message brokers
- Containerization/Virtualization
- GraphQL
- Graph DBs
- WebSockets
- WebServers
- Mitigation strategies
- Migration strategies
- Performance optimization
- Benchmarking
- Observability
- Monitoring
- Telemetry

</p>
</details>

### [DEVOPS](devops/README.md)
<details>
<summary>
Content    
</summary>
<p>
    
- OS Concepts
- Basic system administration
- Different operating systems
- Text manipulation tools
- Bash scripting
- Compiling from source
- Process monitoring
- Network utilities
- System performance
- Networking
- Security
- Protocols
- Reverse proxy
- Forward proxy
- Caching server
- Load balancer
- Firewall
- Web servers
- Configuration management
- Infrastructure provisioning
- Containers
- Container orchestration
- CI/CD tools
- Infrastructure monitoring
- Application monitoring
- Logs management
- Cloud providers
- Cloud design patterns

</p>
</details>
