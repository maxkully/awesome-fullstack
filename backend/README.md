## [Backend Roadmap](README.md)

#### [Algorithms / Data structures](../common/README.md#data-structures-algorithms)
#### [Design Patterns](../common/README.md#design-patterns)
#### [Design principles](../common/README.md#design-principles)
#### OS
- OS concepts (how it works)
- Process management
- Memory management
- InterProcess communication
- I/O management
- Sockets
- POSIX basics
- Virtualization
- Service management
- Startup management
- Concurrency
    - Threads
    - Forks
    - Fibres
- Storage
- Packages/Libraries
- File systems
- GNU Core
- [Terminal Usage](../common/README.md#basic-terminal-usage)
- Hardware
#### Networking
- Concepts
- DNS
#### [HTTP](../common/README.md#httphttps)
#### Web servers
- Nginx
- Apache
- Caddy
- MS IIS
#### Web sockets
#### Architecture
- Architectural patterns
- Development principles
- Microservices
- Monolithic
- SOA
- CQRS and Event Sourcing
- Serverless
- Data Serializing
    - JSON
    - XML
    - YML
#### Message brokers
- AMQP
    - RabbitMQ
    - Kafka
    
https://habr.com/ru/company/itsumma/blog/416629/

#### API
- REST
- SOAP
- HATEOAS
- Open API
- gRPC
- Authentication
    - Cookie Based
    - OAuth
    - Basic
    - Token
    - JWT
    - OpenID
    - SAML
- GraphQL
    - Apollo
    - Relay Modern
#### CI/CD
#### Containerization/Virtualization
#### Languages & Frameworks
- PHP
    - Laravel
    - Symfony
- Ruby
    - Ruby-on-rails
    - Sinatra
- JavaScript
    - Node.js
    - v8
- Python
    - Flask
    - Django
- Golang
#### Databases
- CAP Theorem
- ORM
- ACID
- Transactions
- Indexes
- Normalization (Denormalization)
- Relational DB
    - PostgreSQL
    - MySQL
- NoSQL
    - ODM
    - MongoDB
    - CouchDB
    - DynamoDB
- Graph DBs
    - Neo4j
#### Highload (Performance)
- Benchmarking
- Performance optimization
    - Caching
    - Search engines
    - Replication
    - Sharding
- Horizontal scale
- Vertical scale
#### Fault Tolerance
- Mitigation strategies
    - Graceful degradation
    - Throttling
    - Backpressure
    - Loadshifting
    - Circuit breaker
- Migration strategies
- Observability
    - Monitoring
    - ELK
    - Zabbix
    - Telemetry
#### [Web security](../security/README.md)
#### [Testing](../qa/README.md)
#### Specific skills
- Big Data
- Machine Learning
- Blockchain
- Cryptography
